// System headers.
#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <freertos/semphr.h>
#include "nvs_flash.h"
#include "nvs.h"
#include "protocol_examples_common.h"
#include "tcpip_adapter.h"
#include "lwip/apps/netbiosns.h"
#include "esp_log.h"
#if CONFIG_EXAMPLE_CONNECT_WIFI
#include "esp_wifi.h"
#endif

// Third-party headers.
#include "ssd1306.h"
// #include "font8x8_basic.h"
#include "lora.h"

// Program-specific headers.
#include "sender_sntp.h"

// The ESP tag for distinguishing log entries.
#define tag "lora-sender"

// A mutex to protect access to the data being 
// sent with LoRa, so we can access it for other 
// purposes such as displaying it on the OLED 
// display.
SemaphoreHandle_t xMutex;

// The timestamp data which we send with LoRa.
int hours, minutes, seconds, day, month, year;

// A handle for the OLED display device.
SSD1306_t dev;

// Some OLED display positions for convenience.
int center, top, bottom;

// Initialize the OLED display.
void init_display() {
#if CONFIG_I2C_INTERFACE
	ESP_LOGI(tag, "INTERFACE is i2c");
	ESP_LOGI(tag, "CONFIG_SDA_GPIO=%d",CONFIG_SDA_GPIO);
	ESP_LOGI(tag, "CONFIG_SCL_GPIO=%d",CONFIG_SCL_GPIO);
	ESP_LOGI(tag, "CONFIG_RESET_GPIO=%d",CONFIG_RESET_GPIO);
	i2c_master_init(CONFIG_SDA_GPIO, CONFIG_SCL_GPIO, CONFIG_RESET_GPIO);
#if CONFIG_SSD1306_128x64
	ESP_LOGI(tag, "Panel is 128x64");
	i2c_init(&dev, 128, 64, 0x3C);
#endif // CONFIG_SSD1306_128x64
#if CONFIG_SSD1306_128x32
	ESP_LOGI(tag, "Panel is 128x32");
	i2c_init(&dev, 128, 32, 0x3C);
#endif // CONFIG_SSD1306_128x32
#endif // CONFIG_I2C_INTERFACE

#if CONFIG_SPI_INTERFACE
	ESP_LOGI(tag, "INTERFACE is SPI");
	ESP_LOGI(tag, "CONFIG_CS_GPIO=%d",CONFIG_CS_GPIO);
	ESP_LOGI(tag, "CONFIG_DC_GPIO=%d",CONFIG_DC_GPIO);
	ESP_LOGI(tag, "CONFIG_RESET_GPIO=%d",CONFIG_RESET_GPIO);
	spi_master_init(&dev, CONFIG_CS_GPIO, CONFIG_DC_GPIO, CONFIG_RESET_GPIO);
	spi_init(&dev, 128, 64);
#endif // CONFIG_SPI_INTERFACE

	ssd1306_clear_screen(&dev, false);
	ssd1306_contrast(&dev, 0xff);
#if CONFIG_SSD1306_128x64 || CONFIG_SPI_INTERFACE
	top = 2;
		center = 3;
	bottom = 8;
	ssd1306_display_text(&dev, 0, "SSD1306 128x64", 14, false);
	ssd1306_display_text(&dev, 1, "Initializing...", 15, false);
	ssd1306_clear_line(&dev, 4, true);
	ssd1306_clear_line(&dev, 5, true);
	ssd1306_clear_line(&dev, 6, true);
	ssd1306_clear_line(&dev, 7, true);
	ssd1306_display_text(&dev, 4, "SSD1306 128x64", 14, true);
	ssd1306_display_text(&dev, 5, "Initializing...", 15, true);
#endif // CONFIG_SSD1306_128x64

#if CONFIG_SSD1306_128x32
	top = 1;
	center = 1;
	bottom = 4;
	ssd1306_display_text(&dev, 0, "SSD1306 128x32", 14, false);
	ssd1306_display_text(&dev, 1, "Initializing...", 15, false);
	ssd1306_clear_line(&dev, 2, true);
	ssd1306_clear_line(&dev, 3, true);
	ssd1306_display_text(&dev, 2, "SSD1306 128x32", 14, true);
	ssd1306_display_text(&dev, 3, "Initializing...", 15, true);
#endif // CONFIG_SSD1306_128x32

	return;
}

// A task for transmitting data with LoRa.
void task_tx(void *p)
{
	const char *example_ts = "2020-05-07 10:37:59 EST\0";
	char timestamp_buf[strlen(example_ts) + 18];
	time_t now;

	for(;;) {
		const uint32_t mutexWaitTicks = 1000;
		if (xSemaphoreTake(xMutex, (TickType_t)mutexWaitTicks) == pdTRUE)
		{
			time(&now);
			struct tm *local = localtime(&now);

			hours = local->tm_hour;                                                                             
			minutes = local->tm_min;
			seconds = local->tm_sec;
			day = local->tm_mday;
			month = local->tm_mon + 1;
			year = local->tm_year + 1900;

			sprintf(timestamp_buf, "%d-%02d-%02d %02d:%02d:%02d %s", year, month, day, hours, minutes, seconds, SENDER_SNTP_TIMEZONE);

			lora_send_packet((uint8_t*)timestamp_buf, strlen(timestamp_buf));

			ESP_LOGI(tag, "Data sent: %s", timestamp_buf);

			xSemaphoreGive(xMutex);
		}
		else
		{
			ESP_LOGW(tag, "Couldn't take mutex within %d ticks. The data is currently unavailable. Please try again later.", mutexWaitTicks);
		}

		vTaskDelay(pdMS_TO_TICKS(5000));
	}
}

// A task for displaying the data that's being transmitted 
// with LoRa on the OLED display.
void task_display(void *arg) {
	const char *example_ts = "2020-05-07 10:37:59 EST\0";
	char datestamp_buf[strlen(example_ts) + 1];
	char timestamp_buf[strlen(example_ts) + 1];
	char *line_char_title = "Sent:\0";

	ssd1306_clear_screen(&dev, false);
	ssd1306_contrast(&dev, 0xff);
	ssd1306_display_text(&dev, 0, line_char_title, strlen(line_char_title), false);

	for (;;) {
		const uint32_t mutexWaitTicks = 1000;
		if (xSemaphoreTake(xMutex, (TickType_t)mutexWaitTicks) == pdTRUE)
		{
			sprintf(datestamp_buf, "%d-%02d-%02d", year, month, day);
			sprintf(timestamp_buf, "%02d:%02d:%02d %s", hours, minutes, seconds, SENDER_SNTP_TIMEZONE);

			ESP_LOGI(tag, "Displaying data: %s %s", datestamp_buf, timestamp_buf);

			ssd1306_clear_line(&dev, center, false);
			ssd1306_clear_line(&dev, center + 2, false);

			ssd1306_display_text(&dev, center, datestamp_buf, strlen(datestamp_buf), false);
			ssd1306_display_text(&dev, center + 2, timestamp_buf, strlen(timestamp_buf), false);

			vTaskDelay(pdMS_TO_TICKS(5000));

			xSemaphoreGive(xMutex);
		}
		else
		{
			ESP_LOGW(tag, "Couldn't take mutex within %d ticks. The data is currently unavailable. Please try again later.", mutexWaitTicks);
			vTaskDelay(pdMS_TO_TICKS(5000));
		}
	}
}

// The main entrypoint.
void app_main()
{
	// System init.
	nvs_flash_init();
	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_create_default());

	// Display init.
	init_display();

	// Create mutex for data.
	xMutex = xSemaphoreCreateMutex();
	if (xMutex == NULL)
	{
		ESP_LOGE(tag, "failed creating mutex");
		return;
	}
	
	// Connect to Wifi.
#if CONFIG_EXAMPLE_CONNECT_WIFI
	// Connect.
	ESP_ERROR_CHECK(example_connect());

    // Disable power saving mode to keep Wifi active.
    esp_wifi_set_ps(WIFI_PS_NONE);
#endif

	// SNTP client init.
	sntp_main();

	// LoRa init.
	lora_init();

	// Set LoRa to 915 MHz.
	lora_set_frequency(915e6);

	// Enable LoRa packet validation.
	lora_enable_crc();

	// Start LoRa transmit task.
	xTaskCreate(&task_tx, "task_tx", 2048, NULL, 5, NULL);
	
	// Start OLED display task.
	xTaskCreate(&task_display, "task_display", 4096, NULL, 5, NULL);
}
